$(function () {

    var settings = {
        'client_id': 'HPJOLKVPDKWZH5GI10AR5WY4UB3Q25CCFRZ0ZNWIQVKPJEY3',
        'client_secret': '3ZGO154ZHIEZJOMTAWBRCWHEFFDYBFVQ4R3CM4D03YA3K0PE',
        'v': '20140611'
    }
    var coordinates={};
    var onSuccess = function(position) {
    		coordinates.lat=position.coords.latitude;
            coordinates.lng=position.coords.longitude;
    };
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
    
    function getCategories() {
        return $.ajax({
            url: 'https://api.foursquare.com/v2/venues/categories',
            type: 'GET',
            data: settings,
            dataType: 'json'
        })
    }

    getCategories().done(function (list) {
        $(list.response.categories).each(function (index, element) {
            $('<option>').val($(this)[0].id).text($(this)[0].name).appendTo('#category-list');
        });
    })

    function getPlaces() {
        return $.ajax({
            url: 'https://api.foursquare.com/v2/venues/search',
            type: 'GET',
            data: settings,
            dataType: 'json'
        })
    }

    function createMap() {
        return new GMaps({
            div: '#map',
            lat: coordinates.lat,
            lng: coordinates.lng
        });
    }

    $('#find-places').on('click', function () {
        var btn = $(this);
        btn.addClass("loading");

        settings.categoryId = $('#category-list').val();
        settings.ll = coordinates.lat+","+coordinates.lng;
        getPlaces().done(function (places) {
            $(this).addClass("loading");
            var map = createMap();
            $(places.response.venues).each(function (index, element) {
            	if($(this)[0].location.address){
            		var description = '<div class="map-description text-center"><strong>' + $(this)[0].name + '</strong><br/>' + $(this)[0].location.address + '</a>';
            	}else{
            		var description = '<div class="map-description text-center"><strong>' + $(this)[0].name + '</strong><br/> Adresa nije poznata </a>';
            	}
                map.addMarker({
                    lat: $(this)[0].location.lat,
                    lng: $(this)[0].location.lng,
                    title: $(this)[0].name,
                    infoWindow: {
                        content: description
                    },
                    icon: "img/pin.png",
                });
            });
            btn.removeClass("loading");
        })
    });
});


